import { expect, tap } from '@pushrocks/tapbundle';
import * as gitlab from '../ts/index';

let testGitlabAccount: gitlab.GitlabAccount;

const test1 = tap.test('should create an anonymous Gitlab Account', async () => {
  testGitlabAccount = gitlab.GitlabAccount.createAnonymousAccount();
  expect(testGitlabAccount).to.be.instanceOf(gitlab.GitlabAccount);
});

const test2 = tap.test('should get the pushrocks group', async () => {
  const pushrocksGroup = await testGitlabAccount.getGroupByName('pushrocks');
  expect(pushrocksGroup).to.be.instanceOf(gitlab.GitlabGroup);
  console.log(pushrocksGroup);
});

const test3 = tap.test('should get the pushrocks group', async () => {
  const pushrocksGroup = await testGitlabAccount.getGroupByName('pushrocks');
  expect(pushrocksGroup).to.be.instanceOf(gitlab.GitlabGroup);
  await pushrocksGroup.getProjects();
});

const test4 = tap.test('should get the readme of a project', async () => {
  const pushrocksGroup = await testGitlabAccount.getGroupByName('pushrocks');
  expect(pushrocksGroup).to.be.instanceOf(gitlab.GitlabGroup);
  const projects = await pushrocksGroup.getProjects();
  const selectedProject = projects.find((project) => {
    return project.data?.name === 'smartfile';
  });
  expect(selectedProject.data.name).to.equal('smartfile');
  const readme = await selectedProject.getReadmeAsMarkdown('master');
  expect(readme.startsWith('# @pushrocks/smartfile')).to.be.true;
  return projects;
});

const test5 = tap.test('should be able to create an article list', async () => {
  const projects: gitlab.GitlabProject[] = (await test4.testResultPromise) as any;
  for (const project of projects) {
    console.log(await project.getProjectAsArticle());
  }
})

tap.start();
